<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Files imported
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="flex flex-col mb-8">
                <h1 class="order-1 text-gray-900 text-3xl font-extrabold tracking-tight mt-2">File imported</h1>
                <nav class="flex items-center text-gray-500 text-sm font-medium space-x-2 whitespace-nowrap">
                    <a href="#" class="hover:text-gray-900">
                        Import files
                    </a>
                </nav>
            </div>

            <header class="flex flex-wrap md:flex-nowrap items-center mb-3 py-1.5 whitespace-nowrap">
                <div class="w-full flex-none md:w-auto md:pl-6 mt-1 md:mt-0 ml-auto text-white text-sm font-medium">
                    <a href="{{ route('import.create') }}" class="flex-none uppercasetext-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 text-sm tracking-wide font-semibold px-2 py-1 rounded-md ml-3">
                        Import file →
                    </a>
                </div>
            </header>

            <!-- This example requires Tailwind CSS v2.0+ -->
            <div class="flex flex-col mb-2">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200">
                                <thead class="bg-gray-50">
                                    <tr>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Filename
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Success
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Failed
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Total
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Status
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Imported by
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Summary
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                    @foreach ($imports as $import)
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                            {{ $import->csv_filename }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                            {{ $import->number_created_contacts }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                            {{ $import->number_failed_contacts }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                            {{ $import->number_total_contacts }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            @php
                                                switch($import->import_status->id) {
                                                    case 1:
                                                        $class = "bg-yellow-100 text-yellow-800";
                                                        break;
                                                    case 2:
                                                        $class = "bg-blue-100 text-blue-800";
                                                        break;
                                                    case 3:
                                                        $class = "bg-red-100 text-red-800";
                                                        break;
                                                    case 4:
                                                        $class = "bg-green-100 text-green-800";
                                                        break;
                                                    default:
                                                        $class = "bg-gray-100 text-gray-800";
                                                        break;
                                                }
                                            @endphp
                                            <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full {{ $class }}">
                                                {{ $import->import_status->name }}
                                            </span>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            @if( $import->user )
                                                {{ $import->user->name }}
                                            @endif
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                            <a href="{{ route('import.show', $import->id) }}" class="text-indigo-600 hover:text-indigo-900">Show</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {{ $imports->links() }}

        </div>
    </div>
</x-app-layout>
