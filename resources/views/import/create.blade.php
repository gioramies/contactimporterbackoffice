<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Import file
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <div>
                        <div class="md:grid md:grid-cols-3 md:gap-6">
                            <div class="md:col-span-1">
                                <div class="px-4 sm:px-0">
                                    <h3 class="text-lg font-medium leading-6 text-gray-900">Import Contacts</h3>
                                    <p class="mt-1 text-sm text-gray-600">
                                        Please select a CSV file.
                                    </p>
                                </div>
                            </div>
                            <div class="mt-5 md:mt-0 md:col-span-2">
                                <form class="form-horizontal" method="POST" action="{{ route('import.parse') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="shadow sm:rounded-md sm:overflow-hidden">
                                        <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                            <div>
                                                <label for="csv_file" class="block text-sm font-medium text-gray-700">
                                                    CSV File
                                                </label>
                                                <div class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                                    <div class="space-y-1 text-center">
                                                        <div class="flex text-sm text-gray-900">
                                                            <label for="csv_file" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                                <span>Upload a file</span>
                                                                <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                                                            </label>
                                                        </div>
                                                        <p class="text-xs text-gray-900">
                                                            CSV up to 10MB
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <fieldset>
                                                <div class="space-y-4">
                                                    <div class="flex items-start">
                                                        <div class="flex items-center h-5">
                                                            <input id="header" name="header" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                                        </div>
                                                        <div class="ml-3 text-sm">
                                                            <label for="comments" class="font-medium text-gray-700">Header</label>
                                                            <p class="text-gray-700">File contains header row?</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                                Parse CSV
                                            </button>
                                        </div>
                                    </div>


                                    @if ($errors->has('csv_file'))
                                    <div class="mt-3 flex justify-center p-4 border-2 bg-red-100 border-red-700 rounded-md">
                                        <div class="space-y-1 ">
                                            <div class="flex text-sm text-red-900">
                                                {{ $errors->first('csv_file') }}
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
