<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Import file
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <header class="flex flex-wrap md:flex-nowrap items-center mb-3 py-1.5 whitespace-nowrap">
                <div class="min-w-0 flex items-center">
                    <h2 class="font-medium text-gray-900 truncate">
                        <a href="#component-d60e8c748260b622747ec1568ba9c509" class="mr-1">File: {{ $importFile->csv_filename }}</a>
                    </h2>
                </div>
                <div class="w-full flex-none md:w-auto md:pl-6 mt-1 md:mt-0 ml-auto text-white text-sm font-medium">
                    <a href="{{ route('import.create') }}" class="flex-none uppercasetext-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 text-sm tracking-wide font-semibold px-2 py-1 rounded-md ml-3">
                        Go back →
                    </a>
                </div>
            </header>

            <!-- This example requires Tailwind CSS v2.0+ -->
            <div class="flex flex-col mb-2">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">

                            <div class="m-4 flex justify-center p-4 border-2 bg-green-100 border-green-700 rounded-md">
                                <div class="space-y-1 ">
                                    <div class="flex text-sm text-green-900">
                                        Please match the CSV columns fields with the database column for the Contact. Remember the all the database columns are required. When done please press the button "Import Data" at the bottom of this page.
                                    </div>
                                </div>
                            </div>

                            <form class="form-horizontal" method="POST" action="{{ route('import.process') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="import_file_id" value="{{ $importFile->id }}" />

                                <table class="min-w-full divide-y divide-gray-200">
                                    @if (isset($csvHeaderFieldsArray))
                                    <thead class="bg-gray-50">
                                        <tr>
                                            @foreach ($csvHeaderFieldsArray as $key => $value)
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">{{ $value }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tr>
                                        @foreach ($csvHeaderFieldsArray as $key => $value)
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                <select name="fields_match[{{ $key }}]" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                                    <option value="">Seleccione</option>
                                                    @foreach ($contactTableColumnsArray as $contactColumnIndex => $contactColumnName)
                                                        <option value="{{ (\Request::has('header')) ? $contactColumnIndex : $loop->index }}"
                                                            @if ($key === $contactColumnIndex) selected @endif>{{ $contactColumnName }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        @endforeach
                                    </tr>
                                    @else
                                    <thead class="bg-gray-50">
                                        <tr>
                                            @foreach ($fileData[0] as $key => $value)
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">{{ $key }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tr>
                                        @foreach ($fileData[0] as $key => $value)
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                <select name="fields_match[{{ $key }}]" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                                    <option value="">Seleccione</option>
                                                    @foreach ($contactTableColumnsArray as $contactColumnIndex => $contactColumnName)
                                                        <option value="{{ (\Request::has('header')) ? $contactColumnIndex : $loop->index }}"
                                                            @if ($key === $contactColumnIndex) selected @endif>{{ $contactColumnName }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        @endforeach
                                    </tr>
                                    @endif

                                    <tbody class="bg-white divide-y divide-gray-200">
                                        @foreach ($fileData as $row)
                                            <tr>
                                            @foreach ($row as $key => $value)
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">{{ $value }}</td>
                                            @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        Import Data
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</x-app-layout>
