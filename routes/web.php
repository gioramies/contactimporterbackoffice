<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ContactController;
use App\Http\Controllers\FailedContactController;
use App\Http\Controllers\ImportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => ['auth']], function() {
    Route::post('/import/parse', [ImportController::class, 'parseImport'])->name('import.parse');
    Route::post('/import/process', [ImportController::class, 'processImport'])->name('import.process');
    Route::resource('import', ImportController::class);

    Route::resource('contacts', ContactController::class);

    Route::resource('failedcontacts', FailedContactController::class);
});

require __DIR__.'/auth.php';
