<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\Contact;
use App\Models\FailedContact;
use App\Models\ImportFile;
use App\Models\ImportStatus;

use LVR\CreditCard\CardNumber;

use Auth;
use DB;
use Log;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

use App\Libraries\CreditCardTypeHelper;

class ProcessCsvFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var \App\Models\User
     */
    protected $user;

    /**
     * The user instance.
     *
     * @var \App\Models\ImportFile
     */
    protected $import_file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, ImportFile $import_file)
    {
        $this->user = $user;
        $this->import_file = $import_file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;

        $model = new Contact;
        $contactTableColumnsArray = $model->getColumns();
        if (($key = array_search('user_id', $contactTableColumnsArray)) !== false) {
            unset($contactTableColumnsArray[$key]);
        }

        DB::beginTransaction();

        $importFile = $this->import_file;

        $fieldsMatch = json_decode($importFile->fields_match);

        $importFile->start_processing_date = Carbon::now()->toDateTimeString();

        $fileData = json_decode($importFile->csv_data, true);

        if ($importFile->csv_header) {
            unset($fileData[0]);
        }

        $counterTotal = 0;
        $counterSuccess = 0;
        $counterFailed = 0;

        foreach ($fileData as $row) {

            $counterTotal++;

            $contact = new Contact();

            $input = [];

            // Recorremos todos los campos que tiene el modelo
            $i = 0;
            foreach ($contactTableColumnsArray as $index => $field) {

                if ($importFile->csv_header) {
                    // Por cada campo identificamos el indice de la columna que seleccionaron en los select de la vista
                    if (($key = array_search($index, $fieldsMatch)) !== false) {

                        // Si se le asignó un campo, entonces a esa columna se le asignará el valor que está en el
                        // indice/columna correspondiente del csv
                        if( !empty($row[$key]) ) {
                            $input[$index] = $row[$key];
                        }
                    }
                }
                else {

                    if (($key = array_search($i, $fieldsMatch)) !== false) {
                        // Si se le asignó un campo, entonces a esa columna se le asignará el valor que está en el
                        // indice/columna correspondiente del csv

                        if( !empty($row[$key]) ) {
                            $input[$index] = $row[$key];
                        }
                    }
                }

                $i++;
            }

            // Extraemos la franquicia y los últimos 4 números
            if(isset($input['credit_card_number_encrypted'])) {
                $input['credit_card_last_four_numbers'] = substr($input['credit_card_number_encrypted'], -4);
                $input['credit_card_franchise'] = CreditCardTypeHelper::getType($input['credit_card_number_encrypted']);
            }
            $input['user_id'] = $user->id;
            $input['import_file_id'] = $importFile->id;

            $rules = [
                'name' => 'required|alpha',
                'birthdate' => 'required|date',
                'phone' => 'required|string',
                'address' => 'required|string',
                'credit_card_number_encrypted' => ['required', new CardNumber],
                'credit_card_last_four_numbers' => 'required|digits:4',
                'credit_card_franchise' => 'required|string',
                'email' => 'required|email'
            ];

            $messages = [
                'required' => 'The :attribute field is required.',
                'credit_card' => [
                    'card_checksum_invalid' => 'The credit card number is not valid.'
                ]
            ];

            $validator = Validator::make($input, $rules, $messages, $contactTableColumnsArray);

            if ($validator->fails())
            {
                $errors = $validator->errors();
                $html = "";
                foreach ($errors->all() as $message) { $html .= $message . " "; }

                $input['fail_reason'] = $html;
                $failedContact = FailedContact::create($input);
                $counterFailed++;
            }
            else {
                $contact = Contact::updateOrCreate(['user_id' => $user->id, 'email' => $input['email']], $input);
                $counterSuccess++;
            }

        }

        $importFile->number_total_contacts = $counterTotal;
        $importFile->number_created_contacts = $counterSuccess;
        $importFile->number_failed_contacts = $counterFailed;

        if( $counterTotal == 0 ) {
            $importFile->import_status_id = 4;
        }
        elseif ( $counterSuccess > 0 ) {
            $importFile->import_status_id = 4;
        }
        elseif ( $counterTotal == $counterFailed ) {
            $importFile->import_status_id = 2;
        }

        $importFile->end_processing_date = Carbon::now()->toDateTimeString();
        $importFile->update();

        DB::commit();
    }
}
