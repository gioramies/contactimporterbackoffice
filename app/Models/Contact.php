<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'import_file_id',
        'name',
        'birthdate',
        'phone',
        'address',
        'credit_card_number_encrypted',
        'credit_card_last_four_numbers',
        'credit_card_franchise',
        'franchise',
        'email'
    ];

    protected $dates = [
        'birthdate'
    ];

    public function getColumns()
    {
        return [
            'name' => 'Name',
            'birthdate' => 'Date of Birth',
            'phone' => 'Phone',
            'address' => 'Address',
            'credit_card_number_encrypted' => 'Credit Card',
            'email' => 'Email'
        ];
    }

    public function setCreditCardNumberEncryptedAttribute($value)
    {
        if ($value) {
            $this->attributes['credit_card_number_encrypted'] = Hash::needsRehash($value) ? Hash::make($value) : $value;
        }
    }

    public function setCreditCardLastFourNumbersAttribute($value)
    {
        if ($value) {
            $this->attributes['credit_card_last_four_numbers'] = Crypt::encryptString($value);
        }
    }

    public function getCreditCardLastFourNumbersAttribute($value)
    {
        return Crypt::decryptString($value);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function import_file()
    {
        return $this->belongsTo('App\Models\ImportFile', 'import_file_id');
    }
}
