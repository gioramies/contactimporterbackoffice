<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportFile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'import_status_id',
        'csv_filename',
        'csv_header',
        'csv_data',
        'file_url',
        'number_total_contacts',
        'number_created_contacts',
        'number_failed_contacts',
        'start_processing_date',
        'end_processing_date',
        'fields_match'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function import_status()
    {
        return $this->belongsTo('App\Models\ImportStatus', 'import_status_id');
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact', 'import_file_id');
    }

    public function failed_contacts()
    {
        return $this->hasMany('App\Models\FailedContact', 'import_file_id');
    }
}
