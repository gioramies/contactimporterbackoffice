<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CsvImportRequest;

use App\Jobs\ProcessCsvFileJob;

use App\Models\User;
use App\Models\Contact;
use App\Models\FailedContact;
use App\Models\ImportFile;
use App\Models\ImportStatus;

use LVR\CreditCard\CardNumber;

use Auth;
use DB;
use Log;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

use App\Libraries\CreditCardTypeHelper;

class ImportController extends Controller
{

    public function index()
    {
        $user = Auth::user();

        $imports = ImportFile::with('user', 'import_status')->where('user_id', $user->id)->paginate(15);

        return view('import.index', [
            'imports' => $imports
        ]);
    }

    public function create()
    {
        return view('import.create');
    }

    public function show($id)
    {
        $user = Auth::user();

        $importFile = ImportFile::with('user', 'import_status')->where('user_id', $user->id)->findOrFail($id);

        $contacts = Contact::where('import_file_id', $importFile->id)->where('user_id', $user->id)->paginate(10, '*', 'contacts');
        $failedContacts = FailedContact::where('import_file_id', $importFile->id)->where('user_id', $user->id)->paginate(10, '*', 'failed_contacts');

        return view('import.show', compact('importFile', 'contacts', 'failedContacts'));
    }

    public function parseImport(CsvImportRequest $request)
    {
        $user = Auth::user();

        $path = $request->file('csv_file')->getRealPath();

        $data = array_map('str_getcsv', file($path));

        $csvHeaderFieldsArray = [];

        if (count($data) > 0) {

            $fileData = $data;

            if ($request->has('header')) {
                foreach ($data[0] as $key => $value) {
                    $csvHeaderFieldsArray[$key] = $value;
                }
                unset($fileData[0]);
            }
            else {
                foreach ($data[0] as $key => $value) {
                    $csvHeaderFieldsArray[$key] = $key;
                }
            }

            $importFile = ImportFile::create([
                'user_id' => $user->id,
                'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
                'csv_header' => $request->has('header'),
                'csv_data' => json_encode($data)
            ]);
        }
        else {
            return redirect()->back();
        }

        $model = new Contact;
        $contactTableColumnsArray = $model->getColumns();
        if (($key = array_search('user_id', $contactTableColumnsArray)) !== false) {
            unset($contactTableColumnsArray[$key]);
        }

        return view('import.match', compact( 'csvHeaderFieldsArray', 'fileData', 'importFile', 'contactTableColumnsArray'));

    }

    public function processImport(Request $request)
    {
        $user = Auth::user();

        $model = new Contact;
        $contactTableColumnsArray = $model->getColumns();
        if (($key = array_search('user_id', $contactTableColumnsArray)) !== false) {
            unset($contactTableColumnsArray[$key]);
        }

        $importFile = ImportFile::where('user_id', $user->id)->findOrFail($request->import_file_id);

        $importFile->fields_match = json_encode($request->fields_match);
        $importFile->update();


        $responseMessage = 'The file was imported succesfully! Please refresh this page to see the status of the proccess.';

        try {
            ProcessCsvFileJob::dispatch($user, $importFile);
        }
        catch (\Throwable $e)
        {
            $responseMessage = $e->getMessage();

            Log::error("SendMailJob Throwable: $responseMessage");
        }
        catch (\Swift_TransportException $e)
        {
            $responseMessage = $e->getMessage();

            Log::error("SendMailJob Swift_TransportException: $responseMessage");
        }
        catch (\Swift_RfcComplianceException $e)
        {
            $responseMessage = $e->getMessage();

            Log::error("SendMailJob Swift_RfcComplianceException: $responseMessage");
        }
        catch (\Exception $e)
        {
            $responseMessage = $e->getMessage();

            Log::error("SendMailJob Exception: $responseMessage");
        }

        /*

        $fieldsMatch = json_decode($importFile->fields_match);

        DB::beginTransaction();

        $importFile->start_processing_date = Carbon::now()->toDateTimeString();

        $fileData = json_decode($importFile->csv_data, true);

        if ($importFile->csv_header) {
            unset($fileData[0]);
        }

        $counterTotal = 0;
        $counterSuccess = 0;
        $counterFailed = 0;

        foreach ($fileData as $row) {

            $counterTotal++;

            $contact = new Contact();

            $input = [];

            // Recorremos todos los campos que tiene el modelo
            $i = 0;
            foreach ($contactTableColumnsArray as $index => $field) {

                if ($importFile->csv_header) {
                    // Por cada campo identificamos el indice de la columna que seleccionaron en los select de la vista
                    if (($key = array_search($index, $fieldsMatch)) !== false) {

                        // Si se le asignó un campo, entonces a esa columna se le asignará el valor que está en el
                        // indice/columna correspondiente del csv
                        if( !empty($row[$key]) ) {
                            $input[$index] = $row[$key];
                        }
                    }
                }
                else {

                    if (($key = array_search($i, $fieldsMatch)) !== false) {
                        // Si se le asignó un campo, entonces a esa columna se le asignará el valor que está en el
                        // indice/columna correspondiente del csv

                        if( !empty($row[$key]) ) {
                            $input[$index] = $row[$key];
                        }
                    }
                }

                $i++;
            }

            // Extraemos la franquicia y los últimos 4 números
            if(isset($input['credit_card_number_encrypted'])) {
                $input['credit_card_last_four_numbers'] = substr($input['credit_card_number_encrypted'], -4);
                $input['credit_card_franchise'] = CreditCardTypeHelper::getType($input['credit_card_number_encrypted']);
            }
            $input['user_id'] = $user->id;
            $input['import_file_id'] = $importFile->id;

            $rules = [
                'name' => 'required|alpha',
                'birthdate' => 'required|date',
                'phone' => 'required|string',
                'address' => 'required|string',
                'credit_card_number_encrypted' => ['required', new CardNumber],
                'credit_card_last_four_numbers' => 'required|digits:4',
                'credit_card_franchise' => 'required|string',
                'email' => 'required|email'
            ];

            $messages = [
                'required' => 'The :attribute field is required.',
                'credit_card' => [
                    'card_checksum_invalid' => 'The credit card number is not valid.'
                ]
            ];

            $validator = Validator::make($input, $rules, $messages, $contactTableColumnsArray);

            if ($validator->fails())
            {
                $errors = $validator->errors();
                $html = "";
                foreach ($errors->all() as $message) { $html .= $message . " "; }

                $input['fail_reason'] = $html;
                $failedContact = FailedContact::create($input);
                $counterFailed++;
            }
            else {
                $contact = Contact::updateOrCreate(['user_id' => $user->id, 'email' => $input['email']], $input);
                $counterSuccess++;
            }

        }

        $importFile->number_total_contacts = $counterTotal;
        $importFile->number_created_contacts = $counterSuccess;
        $importFile->number_failed_contacts = $counterFailed;

        if( $counterTotal == 0 ) {
            $importFile->import_status_id = 4;
        }
        elseif ( $counterSuccess > 0 ) {
            $importFile->import_status_id = 4;
        }
        elseif ( $counterTotal == $counterFailed ) {
            $importFile->import_status_id = 2;
        }

        $importFile->end_processing_date = Carbon::now()->toDateTimeString();
        $importFile->update();

        DB::commit();
        */

        $request->session()->flash('message', $responseMessage);

        return redirect()->route('import.show', $importFile->id);
    }
}
