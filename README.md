## Laravel Technical Assessment PHP Koombea

Simple project showing how to import data from CSV file, also matching CSV columns with database columns.

Also showing how to deal with CSV files with/without header rows.


---

### How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate__
- That's it - load the homepage

---

### License

Please use and re-use however you want.

---

## Developer

- Giordano Nobles
