<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_files', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('import_status_id')->default(1);
            $table->foreign('import_status_id')->references('id')->on('import_statuses')->onUpdate('cascade')->onDelete('cascade');
            $table->string('csv_filename');
            $table->boolean('csv_header')->default(0);
            $table->longText('csv_data');
            $table->string('file_url')->nullable();
            $table->unsignedInteger('number_total_contacts')->nullable();
            $table->unsignedInteger('number_created_contacts')->nullable();
            $table->unsignedInteger('number_failed_contacts')->nullable();
            $table->dateTime('start_processing_date')->nullable();
            $table->dateTime('end_processing_date')->nullable();
            $table->string('fields_match')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_files');
    }
}
