<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFailedContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_contacts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('import_file_id');
            $table->foreign('import_file_id')->references('id')->on('import_files')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('birthdate')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('credit_card_number_encrypted')->nullable();
            $table->string('credit_card_last_four_numbers')->nullable();
            $table->string('credit_card_franchise')->nullable();
            $table->string('email')->nullable();
            $table->string('fail_reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_contacts');
    }
}
