<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('import_file_id');
            $table->foreign('import_file_id')->references('id')->on('import_files')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name');
            $table->string('birthdate');
            $table->string('phone');
            $table->string('address');
            $table->string('credit_card_number_encrypted')->nullable();
            $table->string('credit_card_last_four_numbers')->nullable();
            $table->string('credit_card_franchise')->nullable();
            $table->string('email');
            $table->unique(['user_id', 'email']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
