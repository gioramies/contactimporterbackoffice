<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use DB;

class ImportStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('import_statuses')->insert([
            [
                'name' => 'On Hold',
                'description' => 'The file has been uploaded but has not yet started processing.',
            ], [
                'name' => 'Processing',
                'description' => 'The file is actually being read and the contacts are being saved.',
            ], [
                'name' => 'Failed',
                'description' => 'Not a single contact could be imported due to data errors (If the contact file is empty it should not be considered as failed).',
            ], [
                'name' => 'Finished',
                'description' => 'The whole file has been processed and at least one contact has been registered in the database.',
            ]
        ]);
    }
}
