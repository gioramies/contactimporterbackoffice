<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Jhon Doe',
                'email' => 'jhon.doe@email.com',
                'password' => Hash::make('123456Ab'),
            ]
        ]);
    }
}
